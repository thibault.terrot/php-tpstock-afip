<div id="csv">
    <h2><i class="fa fa-list"></i> Importer - Exportation CSV:</h2>
    <div class="form_csv">
        <form name="export_csv" method="post" action="export_csv.php">
            <input type="submit" title="Permet de télécharger un fichier csv contenant la BDD" class="btn btn-primary" value="Exporter la BDD"/>
        </form>
        <br/>
        <br/>
        <?php
            if(isset($tableau_html)){ 
                //On affiche le tableau de vérification
                echo '<div>'.$tableau_html.'</div>';
            }
        ?>
        <form enctype="multipart/form-data" name="check_csv" method="post" action="import_csv.php">
            <input type="file" name="csv_import"/>
            <input type="submit" class="btn btn-primary" name="check" title="Génère un tableau permettant de vérifier le fichier CSV avant de l'importer dans la BDD" value="Vérifier le CSV"/>
            <input type="submit" class="btn btn-primary" name="combine" title="Combiner la BDD et le csv importé en fonction de la référence, désignation, zone, lieu et date de chaque produit" value="Combiner"/>
            <input type="submit" class="btn btn-danger" name="valid" title="Attention, cela remplacera toute la BDD par le fichier CSV" value="Remplacer"/>
        </form>
    </div>
    <br/>
</div>