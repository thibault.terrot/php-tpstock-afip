<?php

    if($fh = fopen("src/BDD/BDD.xml","r")){
        while (!feof($fh)){
            $all_line[] = strip_tags(trim(fgets($fh))); //trim et strip_tags enlevent les espaces et balises
        }
        fclose($fh);
    }
    $BDD = fopen("src/csv/BDD_export.csv", "w");
    $cpt = 0;
    $nb_ligne = 0;
    $all_line = array_values(array_filter($all_line));
    foreach($all_line as $elem){ //On reecrit chaque ligne du tableau dans le csv
        $nb_ligne += 1;
        if($elem != ""){
            $cpt++;
            fwrite($BDD,$elem.";");
            if ($cpt == 8){ //8 est le nombre d'éléments pour un produit (pour savoir quand passer à la ligne suivante et au produit suivant)
                $cpt = 0;
                if(isset($all_line[$nb_ligne+1]))
                { 
                    fwrite($BDD,"\n");
                }
            }
        }
    }
    //On supprime la ligne en plus à la fin (cela peut creer une erreur si on l'importe)

    $export = "done";
    //Téléchargement:
    $file_url = 'src/csv/BDD_export.csv';
    header('Content-Type: application/csv; charset=UTF-8');
    header("Content-disposition: attachment; filename=\"Export_BDD ". date("d_m_Y H_i").".csv\""); 
    readfile($file_url);
?>