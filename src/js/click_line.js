
//Permet de sélectionner une ligne et de la remplir dans les champs pour l'ajout/modification
function click_line(obj){
    var text_line = ($(obj).text()).trim();
    var tab_line = text_line.split("\n");

    $('input[name="ref_modif"]').val(tab_line[0]);
    $('input[name="desi_modif"]').val(tab_line[1]);
    $('input[name="lieu_modif"]').val(tab_line[2]);
    $('input[name="zone_modif"]').val(tab_line[3]);
    document.querySelector('input[name="date_modif"]').value = tab_line[5];
    $('input[name="qty_modif"]').val(parseInt(tab_line[4]));
    $('input[name="prix_modif"]').val(tab_line[6]);
}


//Permet de supprimer une ligne (utilisation d'ajax pour appeler une fonction php)
function delete_line(obj){
    var text_line = ($(obj).text()).trim();
    var tab_line = text_line.split("\n");

    jQuery.ajax({
        type: "POST",
        url: 'del_produit.php',
        data: {line: $(obj).parent().text()},
    });


    $(obj).parent().remove();
   
    
}


  