<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
    <body>
      <div id="liste">
        <h2><i class="fas fa-cubes"></i> Liste des produits:</h2><br/>
        <table class="table table-striped">
            <tr>
              <th>Photo</th>
              <th>Référence</th>
              <th>Désignation</th>
              <th>Lieu de stockage</th>
              <th>Zone de stockage</th>
              <th>Quantité</th>
              <th>Date d'achat</th>
              <th>Prix d'achat</th>
              <th></th>
            </tr>
          <xsl:apply-templates/> 
        </table>
      </div>
    </body>
  </html>
</xsl:template>

<xsl:template match="produit">
  <tr onclick="click_line(this)" id="click">
    <xsl:apply-templates select="photo"/>  
    <xsl:apply-templates select="reference"/>  
    <xsl:apply-templates select="designation"/>
    <xsl:apply-templates select="lieu"/>
    <xsl:apply-templates select="zone"/>
    <xsl:apply-templates select="quantite"/>
    <xsl:apply-templates select="date_achat"/>
    <xsl:apply-templates select="prix_achat"/>
    <td title="Supprimer" onclick="delete_line(this)" id="delete">
      <!-- <form method="post" action="del_produit.php">
        <button type="submit" name="delete_prod" class="suppr fa fa-trash"></button>
      </form> -->
      <button name="delete_prod" class="suppr fa fa-trash"></button>
    </td>
  </tr>
</xsl:template>

<xsl:template match="photo">
    <td>
      <img width="80px" height="80px"> 
        <xsl:attribute name="src">
          <xsl:value-of select="."/>
        </xsl:attribute>
      </img> 
    </td> 
</xsl:template>

<xsl:template match="reference">
    <td><xsl:value-of select="."/></td> 
</xsl:template>

<xsl:template match="designation">
    <td><xsl:value-of select="."/></td>
</xsl:template>

<xsl:template match="lieu">
    <td><xsl:value-of select="."/></td>
</xsl:template>

<xsl:template match="zone">
    <td><xsl:value-of select="."/></td>
</xsl:template>

<xsl:template match="quantite">
    <td><xsl:value-of select="."/></td>
</xsl:template>

<xsl:template match="date_achat">
    <td><xsl:value-of select="."/></td>
</xsl:template>

<xsl:template match="prix_achat">
    <td><xsl:value-of select="."/></td>
</xsl:template>

</xsl:stylesheet>

