
<div id="recherche">
    <h2><i class="fas fa-search"></i> Rechercher un produit:</h2>
    <br/>
    <?php 
        if(isset($tab_val)){
            echo "<table class='table table-striped'><tr><th>Référence</th><th>Désignation</th><th>Lieu de stockage</th><th>Zone de stockage</th><th>Date d'achat</th><th>Quantité</th><th>Prix d'achat</th><th>Photo</th></tr>";
            $cpt = 0;
            echo "<tr>";
            foreach($tab_val as $elem){
                if(stristr($elem, "<photo>")){//Pour la photo
                    echo "<td>";
                    echo "<img width='80px' height='80px' src='".strip_tags($elem)."'/>";
                    echo "</td>";
                }
                else{
                    echo "<td>";
                    echo $elem;
                    echo "</td>";
                }

                $cpt++;
                if($cpt%8 == 0){
                    echo "</tr><tr>";
                }
            }
            echo "</tr></table>";
        }
    ?>
    <br/>
    <form name="search_product" method="post" action="recherche_produit.php">
        <label for="select-field">Rechercher dans: </label>
        <select name="field" id="select-field">
            <option value="">Tout</option>
            <option value="reference">Référence</option>
            <option value="designation">Désignation</option>
            <option value="lieu">Lieu de stockage</option>
            <option value="zone">Zone de stockage</option>
            <option value="quantite">Quantité</option>
            <option value="date_achat">Date d'achat</option>
            <option value="prix_achat">Prix d'achat</option>
            <option value="photo">Photo</option>
        </select>
        <br/><br/>
        <input name="input" type="text"/>
        <button id="btn_add_prod" class="btn btn-primary">Chercher <i class="fas fa-search"></i></button>
    </form>
    <br/>
</div>

                
