<?php
    require("backup.php");
    backup();
    $line = $_POST['line'];
    if(isset($line) && !empty($line)){
        $tab = explode("\n", trim($line));
        
        if(strpos(file_get_contents("src/BDD/BDD.xml"),"<reference>$tab[0]</reference>
        <designation>$tab[1]</designation>
        <lieu>$tab[2]</lieu>
        <zone>$tab[3]</zone>
        <date_achat>$tab[5]</date_achat>") !== false) {

            //On stock toutes les lignes de BDD.xml dans le tableau all_line
            if($fh = fopen("src/BDD/BDD.xml","r")){
                while (!feof($fh)){
                    $all_line[] = fgets($fh);
                }
                fclose($fh);
            }

            //On parcours le tableau all_line pour trouver les lignes que l'on veut modifier
            $cpt=0;
            foreach($all_line as $elem){
                //On vérifie que ce soit le bon produit
                if($elem == "        <reference>$tab[0]</reference>\r\n" && $all_line[$cpt+1] == "        <designation>".$tab[1]."</designation>\r\n" && $all_line[$cpt+2] == "        <lieu>".$tab[2]."</lieu>\r\n" && $all_line[$cpt+3] == "        <zone>".$tab[3]."</zone>\r\n" && $all_line[$cpt+4] == "        <date_achat>".$tab[5]."</date_achat>\r\n"){
                    //On a trouvé le produit a supprimer
                    unset($all_line[$cpt-1]); //<produit>
                    unset($all_line[$cpt]);
                    unset($all_line[$cpt+1]);
                    unset($all_line[$cpt+2]);
                    unset($all_line[$cpt+3]);
                    unset($all_line[$cpt+4]);
                    unset($all_line[$cpt+5]);
                    unset($all_line[$cpt+6]);
                    unset($all_line[$cpt+7]);
                    unset($all_line[$cpt+8]); //</produit>
                }
                $cpt++;
            }
            $ressource = fopen('src/BDD/BDD.xml', 'w');
            ftruncate($ressource,0); //On enleve tout dans le fichier de la BDD
            fclose($ressource);

            $ressource = fopen('src/BDD/BDD.xml', 'a');
            foreach($all_line as $elem){ //On reecrit chaque ligne du tableau dans le xml
                fwrite($ressource,$elem);
            }
            fclose($ressource);
        }
       
    }
?>