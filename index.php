<?php
    include "header.php";
    include "menu_stock.php";
?>

<html>
    <body>
        <div class="container">
            <?php
                //Liste des alerts
                if(isset($error_csv)){
                    echo '<span class="alert alert-danger"><i class="fas fa-exclamation-triangle"></i>  Veuillez choisir un fichier CSV!</span>';
                }
                if(isset($xml_final)){ 
                    echo ('<span style="margin:2%;" class="alert alert-success">'.date("d/m/Y H:i").' :  Votre fichier a été importé avec succès.</span><br/><br/><br/>');   
                }
                if(isset($combine)){
                    if ($combine) echo ('<span style="margin:2%;" class="alert alert-success">'.date("d/m/Y H:i").' :  La BDD a été mise à jour avec succès.</span><br/><br/><br/>');   
                    else echo ('<span style="margin:2%;" class="alert alert-danger">'.date("d/m/Y H:i").' :  Erreur dans votre fichier ou dans la BDD.</span><br/><br/><br/>');
                }
                if(isset($tableau_html)){ 
                    echo ('<span style="margin:2%;" class="alert alert-success">'.date("d/m/Y H:i").' :  Votre fichier a été uploadé avec succès, veuillez le vérifier dans le <a href="#tableau">tableau</a>.</span><br/><br/><br/>');   
                }
                if(isset($export)){ 
                    echo ('<span style="margin:2%;" class="alert alert-success">'.date("d/m/Y H:i").' :  Votre fichier a été exporté avec succès.</span><br/><br/><br/>');   
                }
            ?>
            <?php
                include "recherche_produit_view.php";
                include "add_produit_view.php";
            ?>
            <br/>
            <br/>
            <?php
                $proc=new XsltProcessor;
                $proc->importStylesheet(DOMDocument::load("src/BDD/BDD.xsl")); //load XSL script
                echo $proc->transformToXML(DOMDocument::load("src/BDD/BDD.xml")); //load XML file and echo  
            ?>
            <?php
                include "csv_view.php";
            ?>
        </div>
        <br/><br/>

        <?php
            include "footer.php";
        ?>
    </body>
</html>