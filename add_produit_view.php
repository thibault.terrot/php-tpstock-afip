
<div id="ajout">
    <h2><i class="fas fa-plus"></i> Ajouter/Modifier un produit:</h2>
    <br/>
    <?php 
        if(isset($Err) and count($Err)){ 
            foreach($Err as $Er){ 
                echo '<span class="alert alert-warning">'.$Er.'</span>  ';
            } 
        }
    ?>
    <br/>
    <br/>
    <form enctype="multipart/form-data" name="add_product" method="post" action="add_produit.php">
        <input type="file" name="img_modif"/>
        Références: <input type="text" name="ref_modif">
        Désignation: <input type="text" name="desi_modif">
        Lieu: <input type="text" name="lieu_modif"><br/><br/>
        Zone: <input type="text" name="zone_modif">
        Date: <input type="date" name="date_modif">
        Quantité: <input value="0" min="1" type="number" name="qty_modif">
        Prix: <input value="0" min="0" step="0.01" type="number" name="prix_modif">
        <br/>
        <br/>
        <input type="submit" class="btn btn-primary" value="Ajouter / Modifier le produit"/>
    </form>
</div>