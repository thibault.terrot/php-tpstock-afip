<?php    
    
    if ($_POST["input"]) {
        $input = $_POST["input"];
    }

    if ($_POST["field"]) {
        $field = $_POST["field"];
    } 
    
    if(isset($input) && isset($field)) $tab_val = chercher($input, $field);
    else if(isset($input)){
        $tab_val = chercher($input);
    } 


    require_once('index.php');



    function chercher($input, $field = false){

        //On créé un premier tableau avec seulement les valeurs (il sert seulement à récupérer les clés pour les valeurs que l'on recherche)
        if($fh = fopen("src/BDD/BDD.xml","r")){
            while (!feof($fh)){
                $all_line_without_tag[] = trim(strip_tags(fgets($fh))); //On enleve les balises pour la recherche
                
            }
            fclose($fh);
        }
        //On créé un deuxieme tableau avec toutes les balises
        if($fh = fopen("src/BDD/BDD.xml","r")){
            while (!feof($fh)){
                $all_line_tag[] = fgets($fh);
                
            }
            fclose($fh);
        }

        $key = array_keys($all_line_without_tag, $input);
        if(isset($key) && $key != false){//On a trouvé la/les valeur dans le tableau à la clé $key
            $tab_val = [];//On stock dans tab_val toutes les lignes que l'on veut afficher

            if ($field != false){//Si on recherche un champ en particulier
                foreach($key as $elem){
                    if(stristr($all_line_tag[$elem], "<".$field.">")){
                        $tab_val = get_tab_val($tab_val, $all_line_tag, $elem);
                    }
                }
            }
            else{//Si non on recherche partout
                foreach($key as $elem){
                    $tab_val = get_tab_val($tab_val, $all_line_tag, $elem);
                }
            }
            
            return $tab_val;
        }
    }



    //Renvois un tableau avec les lignes que l'on cherche
    function get_tab_val($tab_val, $all_line_tag, $elem){
        if(stristr($all_line_tag[$elem], "<reference>")){
            $tab_val[] = $all_line_tag[$elem];
            $tab_val[] = $all_line_tag[$elem+1];
            $tab_val[] = $all_line_tag[$elem+2];
            $tab_val[] = $all_line_tag[$elem+3];
            $tab_val[] = $all_line_tag[$elem+4];
            $tab_val[] = $all_line_tag[$elem+5];
            $tab_val[] = $all_line_tag[$elem+6];
            $tab_val[] = $all_line_tag[$elem+7];
        }else if(stristr($all_line_tag[$elem-1], "<reference>")){
            $tab_val[] = $all_line_tag[$elem-1];
            $tab_val[] = $all_line_tag[$elem];
            $tab_val[] = $all_line_tag[$elem+1];
            $tab_val[] = $all_line_tag[$elem+2];
            $tab_val[] = $all_line_tag[$elem+3];
            $tab_val[] = $all_line_tag[$elem+4];
            $tab_val[] = $all_line_tag[$elem+5];
            $tab_val[] = $all_line_tag[$elem+6];
        }else if(stristr($all_line_tag[$elem-2], "<reference>")){
            $tab_val[] = $all_line_tag[$elem-2];
            $tab_val[] = $all_line_tag[$elem-1];
            $tab_val[] = $all_line_tag[$elem];
            $tab_val[] = $all_line_tag[$elem+1];
            $tab_val[] = $all_line_tag[$elem+2];
            $tab_val[] = $all_line_tag[$elem+3];
            $tab_val[] = $all_line_tag[$elem+4];
            $tab_val[] = $all_line_tag[$elem+5];
        }else if(stristr($all_line_tag[$elem-3], "<reference>")){
            $tab_val[] = $all_line_tag[$elem-3];
            $tab_val[] = $all_line_tag[$elem-2];
            $tab_val[] = $all_line_tag[$elem-1];
            $tab_val[] = $all_line_tag[$elem];
            $tab_val[] = $all_line_tag[$elem+1];
            $tab_val[] = $all_line_tag[$elem+2];
            $tab_val[] = $all_line_tag[$elem+3];
            $tab_val[] = $all_line_tag[$elem+4];
        }else if(stristr($all_line_tag[$elem-4], "<reference>")){
            $tab_val[] = $all_line_tag[$elem-4];
            $tab_val[] = $all_line_tag[$elem-3];
            $tab_val[] = $all_line_tag[$elem-2];
            $tab_val[] = $all_line_tag[$elem-1];
            $tab_val[] = $all_line_tag[$elem];
            $tab_val[] = $all_line_tag[$elem+1];
            $tab_val[] = $all_line_tag[$elem+2];
            $tab_val[] = $all_line_tag[$elem+3];
        }else if(stristr($all_line_tag[$elem-5], "<reference>")){
            $tab_val[] = $all_line_tag[$elem-5];
            $tab_val[] = $all_line_tag[$elem-4];
            $tab_val[] = $all_line_tag[$elem-3];
            $tab_val[] = $all_line_tag[$elem-2];
            $tab_val[] = $all_line_tag[$elem-1];
            $tab_val[] = $all_line_tag[$elem];
            $tab_val[] = $all_line_tag[$elem+1];
            $tab_val[] = $all_line_tag[$elem+2];
        }else if(stristr($all_line_tag[$elem-6], "<reference>")){
            $tab_val[] = $all_line_tag[$elem-6];
            $tab_val[] = $all_line_tag[$elem-5];
            $tab_val[] = $all_line_tag[$elem-4];
            $tab_val[] = $all_line_tag[$elem-3];
            $tab_val[] = $all_line_tag[$elem-2];
            $tab_val[] = $all_line_tag[$elem-1];
            $tab_val[] = $all_line_tag[$elem];
            $tab_val[] = $all_line_tag[$elem+1];
        }

        $tab_final = [];
        foreach($tab_val as $elem){
            if(!stristr($elem, "<photo>")){//Pour la photo
                $tab_final[] = strip_tags($elem);
            }else{
                $tab_final[] =$elem;
            }
        }
        return $tab_final;
    }
?>