<html>
    <nav id="main_menu_top" class="navbar navbar-expand-lg navbar-dark bg-dark">                    
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/index.php"><i class="fas fa-home"></i> Accueil</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#recherche"><i class="fas fa-search"></i> Rechercher un produit</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#ajout"><i class="fas fa-plus"></i> Ajouter/modifier un produit</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#liste"><i class="fas fa-cubes"></i> Liste des Produits</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#csv"><i class="fas fa-list"></i> Importer/Exporter CSV</a>
                </li>
                <li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Liens utiles
						</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="nav-link" href="https://docs.google.com/document/d/1zzySz5zeydPcdyDleIHhhTO8ijmL5urhAyiXncMWcHs/edit">Documentation</a>
							<a class="nav-link" href="https://gitlab.com/thibault.terrot/php-tpstock-afip/">GitLab</a>
						</div>
					</li>
            </ul>
    </nav>
</html>