<?php

    //Fonction pour le backup des BDD
    function backup(){

    if($fh = fopen("src/BDD/BDD.xml","r")){
        while (!feof($fh)){
            $all_line_bdd[] = fgets($fh);//On va mettre dans all_line_bdd les elements de la BDD actuelle
        }
        fclose($fh);
    }

    //Historique des Bases de données
    $ressource = fopen('src/BDD/BDD_backup_history.xml', 'r+');
    $stat = fstat($ressource);
    fseek($ressource, $stat['size']); //On prend le nb de characteres du fichier
    fwrite($ressource,"\n\n\n<!-- Base de données du : ". date("d/m/Y H:i") ." -->\n");
    foreach($all_line_bdd as $elem){ //On reecrit chaque ligne du tableau dans le xml
        fwrite($ressource,$elem);
    }
    fclose($ressource);

    //Base de données backup
    $ressource = fopen('src/BDD/BDD_backup.xml', 'w');
    ftruncate($ressource,0); //On enleve tout dans le fichier de la BDD_backup
    fclose($ressource);

    $ressource = fopen('src/BDD/BDD_backup.xml', 'a');
    foreach($all_line_bdd as $elem){ //On reecrit chaque ligne du tableau dans le xml
        fwrite($ressource,$elem);
    }
    fclose($ressource);
    }
?>