<?php
    require("backup.php");
    $targetdir = 'src/csv/';
    $csv_import = $targetdir.basename($_FILES['csv_import']['name']);
    if (move_uploaded_file($_FILES['csv_import']['tmp_name'], $csv_import)) //Si ya un fichier uploadé
    {
        if($fh = fopen($csv_import,"r")){
            while (!feof($fh)){
                $line_csv[] = fgets($fh);//On créé un tableau de ligne du csv
            }
            fclose($fh);
        }

        if(isset($line_csv)){
            if (isset($_POST["check"])) { // Si on a cliqué sur le bouton vérifié
                $tableau_html = verification($line_csv);
                
            }
            else if(isset($_POST["valid"])) { //Si on a cliqué sur le bouton valider
                $xml_final = valider($line_csv);
            }
            else if(isset($_POST["combine"])) { //Si on a cliqué sur le bouton combiner
                $combine = combine($line_csv);
            }
        }else{
            $error_csv = true;
        }        
    }
    require_once('index.php');


    function valider($line_csv){
        backup();

        $xml_final="<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<?xml-stylesheet type=\"text/xsl\" href=\"BDD.xsl\"?>\n<stock>";
        if(isset($line_csv)){
            foreach($line_csv as $elem){ //On reecrit chaque ligne du tableau dans le xml
                $chaine_courante = explode(';', $elem);
                $xml_final = $xml_final."
    <produit>
        <reference>".$chaine_courante[0]."</reference>
        <designation>".$chaine_courante[1]."</designation>
        <lieu>".$chaine_courante[2]."</lieu>
        <zone>".$chaine_courante[3]."</zone>
        <date_achat>".$chaine_courante[4]."</date_achat>
        <quantite>".$chaine_courante[5]."</quantite>
        <prix_achat>".$chaine_courante[6]."</prix_achat>
        <photo>".$chaine_courante[7]."</photo>
    </produit>";
            }    
        }
        $xml_final = $xml_final."\n</stock>";

        $ressource = fopen('src/BDD/BDD.xml', 'w');
        ftruncate($ressource,0); //On enleve tout dans le fichier de la BDD
        fwrite($ressource,$xml_final);
        fclose($ressource);
        return $xml_final;
    }

    function verification($line_csv){
        $table_html = ("<div><h3>Contenu du fichier csv:</h3><br/><table id='tableau' class=\"table table-striped table-bordered\">
        <tr>
            <th>Référence</th>
            <th>Désignation</th>
            <th>Lieu de stockage</th>
            <th>Zone de stockage</th>
            <th>Date d'achat</th>
            <th>Quantité</th>
            <th>Prix d'achat</th>
            <th>Photo</th>
        </tr>");

        if(isset($line_csv)){
            foreach($line_csv as $elem){
                $chaine_courante = explode(';', $elem);
                $table_html = $table_html."
                <tr>
                    <td>".$chaine_courante[0]."</td>
                    <td>".$chaine_courante[1]."</td>
                    <td>".$chaine_courante[2]."</td>
                    <td>".$chaine_courante[3]."</td>
                    <td>".$chaine_courante[4]."</td>
                    <td>".$chaine_courante[5]."</td>
                    <td>".$chaine_courante[6]."</td>
                    <td>".$chaine_courante[7]."</td>
                </tr>";
            }
        }
        $table_html = $table_html."</table></div><br/>";
        return $table_html;
    }


    function combine($line_csv){
        backup();

        //FONCTION Pour l'import, on modifie les produits dans le fichier si ils y sont deja au lieu de supprimer toute la bdd
        if($fh = fopen("src/BDD/BDD.xml","r")){
            while (!feof($fh)){
                $all_line_bdd[] = fgets($fh);//On va mettre dans all_line_bdd les elements de la BDD actuelle puis les elements du csv en verifiant s'ils n'y sont pas deja
            }
            fclose($fh);
        }
        array_pop($all_line_bdd); //On supprime le </stock> (on l' rajoute a la fin)
        if(isset($line_csv)){
            foreach($line_csv as $elem){ //On reecrit chaque ligne du tableau dans le xml
                $chaine_courante = explode(';', $elem);
                $all_line_csv[] = "    <produit>\r\n";
                $all_line_csv[] = "        <reference>".$chaine_courante[0]."</reference>\r\n";
                $all_line_csv[] = "        <designation>".$chaine_courante[1]."</designation>\r\n";
                $all_line_csv[] = "        <lieu>".$chaine_courante[2]."</lieu>\r\n";
                $all_line_csv[] = "        <zone>".$chaine_courante[3]."</zone>\r\n";
                $all_line_csv[] = "        <date_achat>".$chaine_courante[4]."</date_achat>\r\n";
                $all_line_csv[] = "        <quantite>".$chaine_courante[5]."</quantite>\r\n";
                $all_line_csv[] = "        <prix_achat>".$chaine_courante[6]."</prix_achat>\r\n";
                $all_line_csv[] = "        <photo>".$chaine_courante[7]."</photo>\r\n";
                $all_line_csv[] = "    </produit>\r\n";
            }
        }
               
        $all_line=[];
        if(isset($all_line_bdd) and isset($all_line_csv)){
            $all_line = $all_line_bdd;

            //On parcours le tableau all_line pour trouver les lignes que l'on veut modifier
            $cpt=0;
            for($cpt=0; $cpt < sizeof($all_line_csv); $cpt++){
                $elem = $all_line_csv[$cpt];
                $modif = false;
                if(stristr($elem, "<reference>") && in_array($elem, $all_line))//Si l'element contient <reference> et l'élement du tableau csv est aussi dans le tableau all_line
                { //Alors on verifit si la designation lieu et zone et date sont aussi dans le tableau:
                    if(in_array($all_line_csv[$cpt+1],$all_line) && in_array($all_line_csv[$cpt+2],$all_line) && in_array($all_line_csv[$cpt+3],$all_line) && in_array($all_line_csv[$cpt+4],$all_line))//On vérifit que les valeurs se suivent bien (on peut avoir plusieurs produits identique qui correspondraient, on doit donc vérifier que les lignes sont bien à la suite)
                    {
                        //récupération des clés de référence, désignation, lieu, zone et date pour vérifier qu'elles se suivent bien
                        $key_ref = array_keys($all_line, $all_line_csv[$cpt]);
                        $key_desi = array_keys($all_line, $all_line_csv[$cpt+1]);
                        $key_lieu = array_keys($all_line, $all_line_csv[$cpt+2]);
                        $key_zone = array_keys($all_line, $all_line_csv[$cpt+3]);
                        $key_date = array_keys($all_line, $all_line_csv[$cpt+4]);
                        foreach($key_ref as $key){
                            if(in_array($key+1, $key_desi) && in_array($key+2, $key_lieu) && in_array($key+3, $key_zone) && in_array($key+4, $key_date)){//On vérifit que les clés se suivent, si c'est le cas on a trouvé le produit à modifier
                                array_pop($all_line); //On enleve le <produit> qu'on a mit juste avant vu qu'on va pas ajouter mais modifier
                                $all_line[$key+5] = $all_line_csv[$cpt+5]; //modif quantité
                                $all_line[$key+6] = $all_line_csv[$cpt+6]; //modif prix
                                $all_line[$key+7] = $all_line_csv[$cpt+7]; //modif photo
                                $cpt += 8;
                                $modif = true;
                            }
                        }
                        if(!$modif){//On ajoute dans le tableau all_line
                            $all_line[] = $elem;
                        }
                    }else{//On ajoute dans le tableau all_line
                        $all_line[] = $elem;
                    }
                }else{//On ajoute dans le tableau all_line
                    $all_line[] = $elem;
                }
            } 
            $ressource = fopen('src/BDD/BDD.xml', 'w');
            ftruncate($ressource,0); //On enleve tout dans le fichier de la BDD
            fclose($ressource);

            $ressource = fopen('src/BDD/BDD.xml', 'a');
            foreach($all_line as $elem){ //On reecrit chaque ligne du tableau dans le xml
                fwrite($ressource,$elem);
            }
            fwrite($ressource,"</stock>");
            fclose($ressource);
            return true;
        }
        else return false;
    }
?>