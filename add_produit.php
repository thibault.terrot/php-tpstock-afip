<?php
    require("backup.php");
    $Err = array();
    if (empty($_POST["ref_modif"])) {
        $ref_modif = "";
    } else {
        $ref_modif = $_POST["ref_modif"];
    }
    if (empty($_POST["desi_modif"])) {
        $Err[] = "Désigantion requise";
    } else {
        $desi_modif = $_POST["desi_modif"];
    }
    if (empty($_POST["lieu_modif"])) {
        $lieu_modif = "";
    } else {
        $lieu_modif = $_POST["lieu_modif"];
    }
    if (empty($_POST["zone_modif"])) {
        $zone_modif = "";
    } else {
        $zone_modif = $_POST["zone_modif"];
    }
    if (empty($_POST["qty_modif"])) {
        $Err[] = "Quantité requise";
    } else {
        $qty_modif = $_POST["qty_modif"];
    }
    if (empty($_POST["date_modif"])) {
        $date_modif = "";
    } else {
        $date_modif = $_POST["date_modif"];
    }
    if (empty($_POST["prix_modif"])) {
        $prix_modif = "";
    } else {
        $prix_modif = $_POST["prix_modif"];
    }

    if(count($Err)==0){ //Tous les champs sont remplis
        $targetdir = 'src/img/';
        $img_modif = $targetdir.basename($_FILES['img_modif']['name']);
        if (!(move_uploaded_file($_FILES['img_modif']['tmp_name'], $img_modif))) $img_modif = "src/img/default_product.png";

       
        if(strpos(file_get_contents("src/BDD/BDD.xml"),"<reference>$ref_modif</reference>
        <designation>$desi_modif</designation>
        <lieu>$lieu_modif</lieu>
        <zone>$zone_modif</zone>
        <date_achat>$date_modif</date_achat>") !== false) { //Check si la reference/designation/lieu/zone et date sont deja dans le fichier
            //si reference/designation/lieu/zone et date sont dans le fichier on va modifier ce produit la
            modification($ref_modif,$desi_modif,$img_modif,$lieu_modif,$zone_modif,$qty_modif,$date_modif,$prix_modif);
            
        }
        else{ // Pas trouvé la meme reference/designation/lieu/zone et date dans la BDD => on ajoute
            ajout($ref_modif,$desi_modif,$img_modif,$lieu_modif,$zone_modif,$qty_modif,$date_modif,$prix_modif);
        }
    }
    
    require_once('index.php');



    function ajout($ref_modif,$desi_modif,$img_modif,$lieu_modif,$zone_modif,$qty_modif,$date_modif,$prix_modif){
        backup();

        $ajout = "    <produit>
        <reference>".$ref_modif."</reference>
        <designation>".$desi_modif."</designation>
        <lieu>".$lieu_modif."</lieu>
        <zone>".$zone_modif."</zone>
        <date_achat>".$date_modif."</date_achat>
        <quantite>".$qty_modif."</quantite>
        <prix_achat>".$prix_modif."</prix_achat>
        <photo>".$img_modif."</photo>
    </produit>
</stock>";

            $ressource = fopen('src/BDD/BDD.xml', 'r+');
            $stat = fstat($ressource);
            fseek($ressource, $stat['size'] -8); //On prend le nb de characteres du fichier et on enleve 8 (pour supprimer </stock>)
            fwrite($ressource, $ajout);
            fclose($ressource);
    }



    function modification($ref_modif,$desi_modif,$img_modif,$lieu_modif,$zone_modif,$qty_modif,$date_modif,$prix_modif){
        backup();

        //On stock toutes les lignes de BDD.xml dans le tableau all_line
        if($fh = fopen("src/BDD/BDD.xml","r")){
            while (!feof($fh)){
                $all_line[] = fgets($fh);
            }
            fclose($fh);
        }

        //On parcours le tableau all_line pour trouver les lignes que l'on veut modifier
        $cpt=0;
        foreach($all_line as $elem){
            //On vérifie que ce soit le bon produit
            if($elem == "        <reference>$ref_modif</reference>\r\n" && $all_line[$cpt+1] == "        <designation>".$desi_modif."</designation>\r\n" && $all_line[$cpt+2] == "        <lieu>".$lieu_modif."</lieu>\r\n" && $all_line[$cpt+3] == "        <zone>".$zone_modif."</zone>\r\n" && $all_line[$cpt+4] == "        <date_achat>".$date_modif."</date_achat>\r\n"){
                //On a trouvé le produit a modifier
                $all_line[$cpt+5] = "        <quantite>".$qty_modif."</quantite>\r\n";
                $all_line[$cpt+6] = "        <prix_achat>".$prix_modif."</prix_achat>\r\n";
                if($img_modif != "src/img/default_product.png"){
                    $all_line[$cpt+7] = "        <photo>".$img_modif."</photo>\r\n";
                }
            }
            $cpt++;
        }
        $ressource = fopen('src/BDD/BDD.xml', 'w');
        ftruncate($ressource,0); //On enleve tout dans le fichier de la BDD
        fclose($ressource);

        $ressource = fopen('src/BDD/BDD.xml', 'a');
        foreach($all_line as $elem){ //On reecrit chaque ligne du tableau dans le xml
            fwrite($ressource,$elem);
        }
        fclose($ressource);
    }
?>